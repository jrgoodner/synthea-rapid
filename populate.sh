#!/bin/bash
# Intended to be run via cron at ? frequency in order to add new patient lives to FHIR server record set
#	ex. 0 8 * * * /home/ubuntu/rapid-fhir-test/populate.sh >/dev/null 2>&1

#Empty output dir
rm -r ./output/fhir

#Create 15 new patients
./run_synthea -p 15

#Submit each new patient to FHIR server
FILES=./output/fhir/*
for f in $FILES
do
    # echo "$f"
    SERVER="http://ec2-34-209-100-131.us-west-2.compute.amazonaws.com:8080/fhir/baseDstu3/"

    #Submit new patients to server
	now="$(date)"
    echo "$now || curl -H \"Content-Type: application/fhir+json\" $SERVER --data-binary \"@$f\"" >> ./logs/log.txt
    curl -H "Content-Type: application/fhir+json" $SERVER --data-binary "@$f"

done

echo "-----------" >> ./logs/log.txt


